Repositório dos scripts (`package.py`) de instalação da biblioteca MSL (`msl_core`, `msl_cg` e `msl_mhm`) para o gerenciador de pacotes Spack (https://spack.io/).

**1) Baixando e instalando o Spack:**

```
$ git clone https://github.com/spack/spack.git 
$ cd spack
$ git checkout releases/v0.16
$ . share/spack/setup-env.sh
```

**2) Baixando e instalando o repositório dos scripts MSL para o Spack:**

```
$ cd ..
$ git clone https://gitlab.com/rpsouto-atividades-lncc/msl_spack_repo.git

msl_spack_repo/
├── packages
│   ├── msl_cg
│   │   ├── package.py
│   │   └── __pycache__
│   ├── msl_core
│   │   ├── package.py
│   │   └── __pycache__
│   └── msl_mhm
│       ├── package.py
│       └── __pycache__
├── README.md
└── repo.yaml

$ spack repo add msl_spack_repo/
```

**3) Instalando a `msl_mhm` e suas dependências (`msl_core` e `msl_cg`, inclusive)**

```
$ spack install --source msl_mhm

==> Warning: Missing a source id for msl_core@master
==> Warning: Missing a source id for msl_cg@branch_spack
==> Warning: Missing a source id for msl_mhm@branch_spack
[+] /home/user/spack/opt/spack/linux-debian10-thunderx2/gcc-8.3.0/pkgconf-1.7.3-mesj6isokphub4qz37rweazx5bsyo3fu
[+] /home/user/spack/opt/spack/linux-debian10-thunderx2/gcc-8.3.0/berkeley-db-18.1.40-dc6qmrcegjzbvncwqp4aqjrqs2dyj5bl
[+] /home/user/spack/opt/spack/linux-debian10-thunderx2/gcc-8.3.0/zlib-1.2.11-esq3wgmnic7sck5x6euek6bu36svvnul
[+] /home/user/spack/opt/spack/linux-debian10-thunderx2/gcc-8.3.0/unzip-6.0-qqrsps7t76sddb6zzasd7smv5oz54je4
[+] /home/user/spack/opt/spack/linux-debian10-thunderx2/gcc-8.3.0/ncurses-6.2-ivirudd754tzlwrqyzdcoapxrtu7rly5
[+] /home/user/spack/opt/spack/linux-debian10-thunderx2/gcc-8.3.0/readline-8.0-u5qx3ne3rxjrlgf7d22napsxgtqzlq2u
[+] /home/user/spack/opt/spack/linux-debian10-thunderx2/gcc-8.3.0/lua-5.3.5-mhwdnu4n2pdleinyigdw34a3twn7bg3i
[+] /home/user/spack/opt/spack/linux-debian10-thunderx2/gcc-8.3.0/gdbm-1.18.1-rajgfwpqyenojxrbkfkgbqdzbw3u4cya
[+] /home/user/spack/opt/spack/linux-debian10-thunderx2/gcc-8.3.0/perl-5.32.0-y4ojiflhpg5xqkfogn6tbpkufnfj6oyh
[+] /home/user/spack/opt/spack/linux-debian10-thunderx2/gcc-8.3.0/openssl-1.1.1h-yrkwylysbasd2kz7prtmg4kskcb3cw47
[+] /home/user/spack/opt/spack/linux-debian10-thunderx2/gcc-8.3.0/cmake-3.18.4-d6bpg6sfeck6keslkq5vxbz5xdawz4x4
==> Installing msl_core-master-t2yihql7qijwwdqpgs6u42ryvxqzhya5
==> No binary for msl_core-master-t2yihql7qijwwdqpgs6u42ryvxqzhya5 found: installing from source
==> msl_core: Executing phase: 'cmake'
==> msl_core: Executing phase: 'build'
==> msl_core: Executing phase: 'install'
[+] /home/user/spack/opt/spack/linux-debian10-thunderx2/gcc-8.3.0/msl_core-master-t2yihql7qijwwdqpgs6u42ryvxqzhya5
==> Installing msl_cg-branch_spack-n6ls6kuclk6xtho6ypwsvv7imlp3mz7k
==> No binary for msl_cg-branch_spack-n6ls6kuclk6xtho6ypwsvv7imlp3mz7k found: installing from source
==> msl_cg: Executing phase: 'cmake'
==> msl_cg: Executing phase: 'build'
==> msl_cg: Executing phase: 'install'
[+] /home/user/spack/opt/spack/linux-debian10-thunderx2/gcc-8.3.0/msl_cg-branch_spack-n6ls6kuclk6xtho6ypwsvv7imlp3mz7k
==> Installing msl_mhm-branch_spack-o7pfr4jyrj3skilktg7auws52fvvtkmb
==> No binary for msl_mhm-branch_spack-o7pfr4jyrj3skilktg7auws52fvvtkmb found: installing from source
==> msl_mhm: Executing phase: 'cmake'
==> msl_mhm: Executing phase: 'build'
==> msl_mhm: Executing phase: 'install'
```

**4) Mostrando o caminho onde `msl_mhm` foi instalada**

```
$ spack find -p msl_mhm
==> 1 installed package
-- linux-debian10-thunderx2 / gcc@8.3.0 -------------------------
msl_mhm@branch_spack  /home/user/spack/opt/spack/linux-debian10-thunderx2/gcc-8.3.0/msl_mhm-branch_spack-o7pfr4jyrj3skilktg7auws52fvvtkmb
```

**5) Mostrando o conteúdo do diretório onde `msl_mhm` foi instalada**

```
$ ls -A1 /home/user/spack/opt/spack/linux-debian10-thunderx2/gcc-8.3.0/msl_mhm-branch_spack-o7pfr4jyrj3skilktg7auws52fvvtkmb
include
lib
share
```

**6) Mostrando as dependências de `msl_mhm`**

```
$ spack find -d msl_mhm
==> 1 installed package
-- linux-debian10-thunderx2 / gcc@8.3.0 -------------------------
msl_mhm@branch_spack
    msl_cg@branch_spack
        msl_core@master
            lua@5.3.5
                ncurses@6.2
                readline@8.1
                unzip@6.0
```
