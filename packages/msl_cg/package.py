# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class MslCg(CMakePackage):
    """Library for representing callpaths consistently in
       distributed-memory performance tools."""

    homepage = "https://gitlab.com/ipes"
    git      = "https://gitlab.com/ipes/msl_cg.git"

    version('branch_spack',  branch='branch_spack')
    
    def setup_dependent_environment(self, spack_env, run_env, dependent_spec):
        spack_env.set('MSL_CG_DIR',  join_path(self.prefix,'share','msl_cg','src'))    

    depends_on("cmake@3.2.3:", type="build")
    depends_on("msl_core")

    variant('build_type', default='Debug',
           description='The build type to build',
           values=('Debug', 'Release', 'DebugRelease'))

    def cmake_args(self):
        args = [
        self.define_from_variant('DCMAKE_BUILD_TYPE', 'build_type')
    ]
        return args
