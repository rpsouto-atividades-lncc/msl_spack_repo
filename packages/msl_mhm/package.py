# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class MslMhm(CMakePackage):
    """Library for representing callpaths consistently in
       distributed-memory performance tools."""

    homepage = "https://gitlab.com/ipes"
    git      = "https://gitlab.com/ipes/msl_mhm.git"

    version('branch_spack',  branch='branch_spack')

    depends_on("cmake@3.2.3:", type="build")
    depends_on("msl_cg")
    depends_on("msl_core")

    variant('build_type', default='Debug',
           description='The build type to build',
           values=('Debug', 'Release', 'DebugRelease'))

#    def setup_build_environment(self, env):
#        env.set('TMPDIR', self.prefix)    

#    def setup_build_environment(self, spack_env):
#        spack_env.set('TMPDIR', self.prefix)            

#    def setup_environment(self, spack_env, run_env): 
#        spack_env.set('TMPDIR', self.prefix)
        
#    def setup_run_environment(self, spack_env, run_env): 
#        spack_env.set('TMPDIR', self.prefix)
        
    def cmake_args(self):
        args = [
        self.define_from_variant('DCMAKE_BUILD_TYPE', 'build_type')
    ]
        return args
